{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Enhances GuiTestRunner form with Code Coverage information.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-14  pk  ---  Added header and some documentation.
// 2017-06-13  pk  ---  Wrote first working version with tabs displaying something.
// *****************************************************************************
   )
}

unit PepiMK.CodeCoverage.GuiTestRunner;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   Forms,
   Controls,
   ComCtrls,
   GuiTestRunner,
   PepiMK.CodeCoverage,
   PepiMK.CodeCoverage.GDB;

type

   { TCodeCoverageTabSheet }

   TCodeCoverageTabSheet = class(TTabSheet)
   protected
      FTree: TTreeView;
   end;

   { TCodeCoverageGUITestRunner }

   TCodeCoverageGUITestRunner = class(TGUITestRunner)
   private
      FCodeCoverage: TCodeCoverage;
      {
        DisplayCodeCoverage inserts code coverage information into the
        GuiTestRunner user interface.
      }
      procedure DisplayCodeCoverage;
      {
        ClearTabs removes all code coverage information tabs from the
        GuiTestRunner user interface.
      }
      procedure ClearTabs;
      {
        GetTab returns the code coverage information tab for the specified
        file.
      }
      function GetTab(const AFilename: string): TCodeCoverageTabSheet;
   public
      constructor Create(TheOwner: TComponent); override;
      destructor Destroy; override;
      {
        RunTest enhances the original RunTest code with code coverage
        information processing.
      }
      procedure RunTest(ATest: TTest); override;
   end;

implementation

uses
   Dialogs,
   testregistry,
   Laz2_DOM;

procedure XML2Tree(XMLDoc: TXMLDocument; TreeView: TTreeView);

   function GetNodeAttributesAsString(ANode: TDOMNode): string;
   var
      i: integer;
   begin
      Result := '';
      if ANode.HasAttributes then begin
         for i := 0 to Pred(ANode.Attributes.Length) do begin
            with ANode.Attributes[i] do begin
               Result := Result + format(' %s="%s"', [NodeName, NodeValue]);
            end;
         end;
      end;
      Result := Trim(Result);
   end;

   procedure ParseXML(ANode: TDOMNode; ATreeNode: TTreeNode);
   var
      sNodeName: string;
   begin
      if ANode = nil then begin
         Exit;
      end;
      sNodeName := ANode.NodeName;
      ATreeNode := TreeView.Items.AddChild(ATreeNode, Trim(ANode.NodeName + ' ' + GetNodeAttributesAsString(ANode) + ANode.NodeValue));
      case sNodeName of
         'file':
         begin
            ATreeNode.Text := TDOMElement(ANode).GetAttribute('path') + TDOMElement(ANode).GetAttribute('name');
         end;
         'class':
         begin
            ATreeNode.Text := TDOMElement(ANode).GetAttribute('name');
         end;
         'function', 'method':
         begin
            ATreeNode.Text := TDOMElement(ANode).GetAttribute('signature');
         end;
      end;

      ANode := ANode.FirstChild;
      while ANode <> nil do begin
         ParseXML(ANode, ATreeNode);
         ANode := ANode.NextSibling;
      end;
      case sNodeName of
         'file', 'totals': ATreeNode.Expand(False);
         'class': ATreeNode.Expand(True);
      end;
   end;

begin
   TreeView.Items.Clear;
   if XMLDoc.DocumentElement.ChildNodes.Count > 0 then begin
      ParseXML(XMLDoc.DocumentElement.ChildNodes[0], nil);
   end else begin
      ParseXML(XMLDoc.DocumentElement, nil);
   end;
end;

{ TCodeCoverageGUITestRunner }

procedure TCodeCoverageGUITestRunner.DisplayCodeCoverage;
var
   i: integer;
   t: TCodeCoverageTabSheet;
begin
   for i := 0 to Pred(FCodeCoverage.Documents.Count) do begin
      t := GetTab(FCodeCoverage.Documents.Files[i].SourceFilename);
      XML2Tree(FCodeCoverage.Documents.Files[i], t.FTree);
   end;
end;

procedure TCodeCoverageGUITestRunner.ClearTabs;
var
   i: integer;
   t: TCodeCoverageTabSheet;
begin
   for i := Pred(PageControl1.PageCount) downto 2 do begin
      if PageControl1.Pages[i] is TCodeCoverageTabSheet then begin
         t := TCodeCoverageTabSheet(PageControl1.Pages[i]);
         t.PageControl := nil;
         t.Parent := nil;
         t.Free;
      end;
   end;
end;

function TCodeCoverageGUITestRunner.GetTab(const AFilename: string): TCodeCoverageTabSheet;
begin
   Result := TCodeCoverageTabSheet.Create(nil);
   Result.Parent := PageControl1;
   Result.PageControl := PageControl1;
   Result.Caption := ExtractFileName(AFilename);
   Result.FTree := TTreeView.Create(Result);
   Result.FTree.Parent := Result;
   Result.FTree.Align := alClient;
   Result.FTree.ReadOnly := True;
end;

constructor TCodeCoverageGUITestRunner.Create(TheOwner: TComponent);
begin
   inherited Create(TheOwner);
   FCodeCoverage := TCodeCoverage.Create;
end;

destructor TCodeCoverageGUITestRunner.Destroy;
begin
   FCodeCoverage.Free;
   inherited Destroy;
end;

procedure TCodeCoverageGUITestRunner.RunTest(ATest: TTest);
begin
   XMLSynEdit.BorderSpacing.Around := 0;
   TestTree.BorderSpacing.Top := 0;
   PageControl1.BorderSpacing.Around := 6;
   ClearTabs;
   FCodeCoverage.PreProcessCodeCoverage;
   inherited RunTest(ATest);
   FCodeCoverage.PostProcessCodeCoverage;
   DisplayCodeCoverage;
end;

end.
