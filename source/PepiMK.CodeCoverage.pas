{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Create code coverage reports for the current FPCUnit test suite.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-15  pk   1h  Implemented source units lookup.
// 2017-06-14  pk   1h  Implemented testcase units lookup.
// 2017-06-12  pk  ---  Started draft
// *****************************************************************************
   )
}

unit PepiMK.CodeCoverage;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Laz2_DOM,
   PParser,
   PasTree,
   contnrs,
   fpcunit,
   Windows,
   PepiMK.CodeCoverage.FileWriter,
   PepiMK.CodeCoverage.GDB;

type
   TOnParserEngineCreateElement = procedure(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
      const ASourceFilename: string; ASourceLinenumber: integer) of object;

   { TCodeCoverage }

   TCodeCoverage = class
   private
      FDocuments: TCodeCoverageFiles;
      FSourceUnits: TStringList;
      FTestCaseUnits: TStringList;
      FClassAssociations: TStringList;
      FFunctionAssociations: TStringList;
      FCoveredThings: TStringList;
      {
        FindTestUnitFilenames finds the full testcase unit filenames
        from the content of the Gnu Debugger file (.gdb).
      }
      procedure FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
      {
        FindSourceUnitFilenames finds the full tested unit filenames
        from the content of the Gnu Debugger file (.gdb).
      }
      procedure FindSourceUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
      {
        Loads the filenames of the testcase and tested source units.
      }
      procedure LoadUnitFilenames;
      {
        ParseSourceByFilename parses a Pascal source file for classes,
        methods und functions.

        @param(AFilename The name of the Pascal source file.)
      }
      procedure ParseSourceByFilename(AFilename: string);
      {
        ParseTestByFilename parses a Pascal testcase file for a list of
        covered methods und functions, either writing it to the XML,
        or a separate list.

        @param(AFilename The name of the Pascal source file.)
        @param(ACoversList If not nil, will be filled with covered methods and functions.)
      }
      procedure ParseTestByFilename(AFilename: string; ACoversList: TStrings);
      {
        ParseSources loops through all specified source files, parsing them
        by calling ParseSourceByFilename on them.
      }
      procedure ParseSources;
      {
        ParseTests loops through all specified testcasesource files, parsing them
        by calling ParseTestByFilename on them.
      }
      procedure ParseTests(ACoversList: TStrings);
      {
        FindCoveredThings finds all covered methods and functions from the
        comments in the testcase units.
      }
      procedure FindCoveredThings;
   protected
      {
        DoCreateElementInSourceFile is called by the PasTreeContainer and
        required to find all lines in source units.
      }
      procedure DoCreateElementInSourceFile(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
         const ASourceFilename: string; ASourceLinenumber: integer);
   public
      class var TestAssociations: TStringList;
      class constructor Create;
      class destructor Destroy;
   public
      constructor Create;
      destructor Destroy; override;
      {
        PreProcessCodeCoverage prepares code coverage information available
        before running the test.
      }
      procedure PreProcessCodeCoverage;
      {
        PostProcessCodeCoverage takes executed line information and
        combines it with the code coverage report.
      }
      procedure PostProcessCodeCoverage;
      property TestCaseUnits: TStringList read FTestCaseUnits;
      property SourceUnits: TStringList read FSourceUnits;
      property Documents: TCodeCoverageFiles read FDocuments;
   end;

procedure RegisterCodeCoverageTest(ATestClass: TTestCaseClass);

implementation

uses
   testregistry,
   RegExpr;

type

   { TPickupCommentsPasTreeContainer }

   TPickupCommentsPasTreeContainer = class(TPasTreeContainer)
   private
      FOnCreateElement: TOnParserEngineCreateElement;
      FSourceFilename: string;
   public
      function CreateElement(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility; const ASourceFilename: string;
         ASourceLinenumber: integer): TPasElement; override;
      function FindElement(const {%H-}AName: string): TPasElement; override;
      property OnCreateElement: TOnParserEngineCreateElement read FOnCreateElement write FOnCreateElement;
      property SourceFilename: string read FSourceFilename write FSourceFilename;
   end;


function TPickupCommentsPasTreeContainer.CreateElement(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
   const ASourceFilename: string; ASourceLinenumber: integer): TPasElement;
begin
   Result := AClass.Create(AName, AParent);
   Result.Visibility := AVisibility;
   Result.SourceFilename := ASourceFilename;
   Result.SourceLinenumber := ASourceLinenumber;
   Result.DocComment := CurrentParser.SavedComments;
   if Assigned(FOnCreateElement) then begin
      FOnCreateElement(AClass, AName, AParent, AVisibility, ASourceFilename, ASourceLinenumber);
   end;
end;

function TPickupCommentsPasTreeContainer.FindElement(const AName: string): TPasElement;
begin
   Result := nil;
end;

procedure RegisterCodeCoverageTest(ATestClass: TTestCaseClass);
begin
   RegisterTest(ATestClass);
   TCodeCoverage.TestAssociations.Add(ATestClass.ClassName + '=' + ATestClass.UnitName);
end;

{ TCodeCoverage }

procedure TCodeCoverage.FindTestUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);
var
   ts: TTestSuite;
   t: TTest;
   i: integer;
   sUnitName: string;
begin
   ts := GetTestRegistry;
   for i := 0 to Pred(ts.GetChildTestCount) do begin
      t := ts.GetChildTest(i);
      sUnitName := TCodeCoverage.TestAssociations.Values[t.TestName];
      if AGnuDebuggerFile.GetUnitFullFilename(sUnitName) then begin
         TestCaseUnits.Add(sUnitName);
      end;
   end;
end;

procedure TCodeCoverage.FindSourceUnitFilenames(AGnuDebuggerFile: TGnuDebuggerFile);

var
   slAllFiles: TStringList;
   iFile: integer;

   procedure FindInUnit(AFilename: string);

      procedure ProcessThing(AThing: string);
      var
         iThing, iFilename: integer;
      begin
         iThing := FCoveredThings.IndexOf(AThing);
         if iThing > -1 then begin
            iFilename := FSourceUnits.IndexOf(AFilename);
            if iFilename < 0 then begin
               FSourceUnits.Add(AFilename);
            end;
         end;
      end;

      procedure ProcessElement(AElement: TPasElement);
      begin
         if AElement is TPasClassType then begin
            ProcessThing(AElement.Name);
         end;
         if AElement is TPasProcedure then begin
            ProcessThing(AElement.Name);
         end;
      end;

   var
      ptc: TPickupCommentsPasTreeContainer;
      pm: TPasModule;
      d: TFPList;
      i: integer;
      pe: TPasElement;
   begin
      if Pos('\lazarus\lcl\', AFilename) > 0 then begin
         Exit;
      end;
      if Pos('\lazarus\components\', AFilename) > 0 then begin
         Exit;
      end;
      OutputDebugString(PChar(Format('Parsing (%d/%d) %s...', [iFile + 1, slAllFiles.Count, slAllFiles[iFile]])));
      try
         ptc := TPickupCommentsPasTreeContainer.Create;
         try
            ptc.SourceFilename := AFilename;
            ptc.OnCreateElement := @DoCreateElementInSourceFile;
            ptc.NeedComments := False;
            pm := ParseSource(ptc, AFilename, 'win32', 'i386');
            try
               if Assigned(pm.InterfaceSection) then begin
                  d := pm.InterfaceSection.Declarations;
                  for i := 0 to Pred(d.Count) do begin
                     pe := (TObject(d[i]) as TPasElement);
                     ProcessElement(pe);
                  end;
               end;
               if Assigned(pm.ImplementationSection) then begin
                  d := pm.ImplementationSection.Declarations;
                  for i := 0 to Pred(d.Count) do begin
                     pe := (TObject(d[i]) as TPasElement);
                     ProcessElement(pe);
                  end;
               end;
            finally
               pm.Free;
            end;
         finally
            ptc.Free;
         end;
      except
         // broken files can happen
      end;
   end;

begin
   slAllFiles := TStringList.Create;
   try
      AGnuDebuggerFile.ListUnitFilenames(slAllFiles);
      for iFile := 0 to Pred(slAllFiles.Count) do begin
         FindInUnit(slAllFiles[iFile]);
      end;
   finally
      slAllFiles.Free;
   end;
end;

procedure TCodeCoverage.ParseSourceByFilename(AFilename: string);
var
   nFile: TDOMNode;
   doc: TCodeCoverageXML;

   procedure ProcessElement(AElement: TPasElement);
   begin
      if AElement is TPasClassType then begin
         doc.FindClassNode(nFile, AElement.Name, True, AElement.SourceLinenumber);
         if FClassAssociations.IndexOfName(AElement.Name) < 0 then begin
            FClassAssociations.AddObject(AElement.Name + '=' + AFilename, doc);
         end;
      end;
      if AElement is TPasProcedure then begin
         doc.AddMethod(nFile, TPasProcedure(AElement).FullName, AElement.GetDeclaration(True), AElement.SourceLinenumber);
         FFunctionAssociations.AddObject(AElement.Name + '=' + AFilename, doc);
      end;
   end;

var
   ptc: TPickupCommentsPasTreeContainer;
   pm: TPasModule;
   d: TFPList;
   i: integer;
   pe: TPasElement;

begin
   doc := FDocuments.FindDocumentByFilename(AFilename);
   try
      nFile := doc.FindFileNode(AFilename);
      ptc := TPickupCommentsPasTreeContainer.Create;
      ptc.SourceFilename := AFilename;
      ptc.OnCreateElement := @DoCreateElementInSourceFile;
      try
         ptc.NeedComments := True;
         pm := ParseSource(ptc, AFilename, 'win32', 'i386');
         try
            if Assigned(pm.InterfaceSection) then begin
               d := pm.InterfaceSection.Declarations;
               for i := 0 to Pred(d.Count) do begin
                  pe := (TObject(d[i]) as TPasElement);
                  ProcessElement(pe);
                  //OutputDebugString(PChar(Format('interface %d: %s (%s) - %s', [i, pe.Name, pe.ClassName, pe.GetDeclaration(True)])));
               end;
            end;
            if Assigned(pm.ImplementationSection) then begin
               d := pm.ImplementationSection.Declarations;
               for i := 0 to Pred(d.Count) do begin
                  pe := (TObject(d[i]) as TPasElement);
                  ProcessElement(pe);
                  //OutputDebugString(PChar(Format('implementation %d: %s (%s) - %s', [i, pe.Name, pe.ClassName, pe.GetDeclaration(True)])));
               end;
            end;
         finally
            pm.Free;
         end;
      finally
         ptc.Free;
      end;
   finally
      doc.Save;
   end;
end;

procedure TCodeCoverage.ParseTestByFilename(AFilename: string; ACoversList: TStrings);

   procedure ProcessCoversStatement(AStatement: string);
   var
      doc: TCodeCoverageXML;
      iXML, iDot: integer;
      sClassName, sMethodName: string;
   begin
      if Assigned(ACoversList) then begin
         ACoversList.Add(AStatement);
         Exit;
      end;
      iDot := Pos('.', AStatement);
      if iDot > 0 then begin
         sClassName := Copy(AStatement, 1, Pred(iDot));
         sMethodName := Copy(AStatement, Succ(iDot));
         iXML := FClassAssociations.IndexOfName(sClassName);
         if iXML > -1 then begin
            doc := TCodeCoverageXML(FClassAssociations.Objects[iXML]);
            doc.RegisterMethodCoveringCode(FClassAssociations.ValueFromIndex[iXML], sClassName, sMethodName);
            doc.Save;
         end;
      end else begin
         iXML := FFunctionAssociations.IndexOfName(AStatement);
         if iXML > -1 then begin
            doc := TCodeCoverageXML(FFunctionAssociations.Objects[iXML]);
            doc.RegisterFunctionCoveringCode(FFunctionAssociations.ValueFromIndex[iXML], AStatement);
            doc.Save;
         end;
      end;
   end;

   procedure ProcessElement(AElement: TPasElement);
   var
      r: TRegExpr;
   begin
      if AElement is TPasProcedure then begin
         if SameText('test', Copy(AElement.Name, 1, 4)) or (Pos('.test', LowerCase(AElement.Name)) > 0) then begin
            r := TRegExpr.Create('@covers\(([^\)]*)\)');
            if r.Exec(AElement.DocComment) then begin
               repeat
                  ProcessCoversStatement(r.Match[1]);
               until not r.ExecNext;
            end;
         end;
      end;
   end;

var
   ptc: TPasTreeContainer;
   pm: TPasModule;
   d: TFPList;
   i: integer;
   pe: TPasElement;
begin
   ptc := TPickupCommentsPasTreeContainer.Create;
   try
      ptc.NeedComments := True;
      pm := ParseSource(ptc, AFilename, 'win32', 'i386');
      try
         if Assigned(pm.InterfaceSection) then begin
            d := pm.InterfaceSection.Declarations;
            for i := 0 to Pred(d.Count) do begin
               pe := (TObject(d[i]) as TPasElement);
               ProcessElement(pe);
            end;
         end;
         if Assigned(pm.ImplementationSection) then begin
            d := pm.ImplementationSection.Declarations;
            for i := 0 to Pred(d.Count) do begin
               pe := (TObject(d[i]) as TPasElement);
               ProcessElement(pe);
            end;
         end;
      finally
         pm.Free;
      end;
   finally
      ptc.Free;
   end;
end;

constructor TCodeCoverage.Create;
begin
   FTestCaseUnits := TStringList.Create;
   FSourceUnits := TStringList.Create;
   FClassAssociations := TStringList.Create;
   FFunctionAssociations := TStringList.Create;
   FCoveredThings := TStringList.Create;
   FDocuments := TCodeCoverageFiles.Create(True);
end;

destructor TCodeCoverage.Destroy;
begin
   FDocuments.Free;
   FTestCaseUnits.Free;
   FSourceUnits.Free;
   FClassAssociations.Free;
   FFunctionAssociations.Free;
   FCoveredThings.Free;
   inherited Destroy;
end;

procedure TCodeCoverage.PreProcessCodeCoverage;
begin
   SourceUnits.Clear;
   TestCaseUnits.Clear;
   LoadUnitFilenames;
end;

procedure TCodeCoverage.PostProcessCodeCoverage;
begin
   FDocuments.Clear;
   FClassAssociations.Clear;
   FFunctionAssociations.Clear;
   ParseSources;
   ParseTests(nil);
end;

procedure TCodeCoverage.LoadUnitFilenames;
var
   dbg: TGnuDebuggerFile;
   sGDBFilename: string;
begin
   dbg := TGnuDebuggerFile.Create;
   try
      sGDBFilename := Copy(ParamStr(0), 1, Length(ParamStr(0)) - Length(ExtractFileExt(ParamStr(0)))) + '.dbg';
      dbg.LoadFromFile(sGDBFilename);
      FindTestUnitFilenames(dbg);
      // DONE : parse them into a function/class/class.method=filename list
      // DONE : get all @covers() information for TestCaseUnits comments or coverage file
      FindCoveredThings;
      FindSourceUnitFilenames(dbg);
   finally
      dbg.Free;
   end;
end;

procedure TCodeCoverage.FindCoveredThings;
begin
   FCoveredThings.Clear;
   ParseTests(FCoveredThings);
end;

procedure TCodeCoverage.ParseSources;
var
   i: integer;
begin
   for i := 0 to Pred(FSourceUnits.Count) do begin
      ParseSourceByFilename(FSourceUnits[i]);
   end;
end;

procedure TCodeCoverage.ParseTests(ACoversList: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(FTestCaseUnits.Count) do begin
      ParseTestByFilename(FTestCaseUnits[i], ACoversList);
   end;
end;

procedure TCodeCoverage.DoCreateElementInSourceFile(AClass: TPTreeElement; const AName: string; AParent: TPasElement; AVisibility: TPasMemberVisibility;
   const ASourceFilename: string; ASourceLinenumber: integer);
var
   doc: TCodeCoverageXML;
begin
   doc := FDocuments.FindDocumentByFilename(ASourceFilename);
   doc.AddLineToken(ASourceFilename, ASourceLinenumber, AClass.ClassName, AName);
end;

class constructor TCodeCoverage.Create;
begin
   TestAssociations := TStringList.Create;
end;

class destructor TCodeCoverage.Destroy;
begin
   TestAssociations.Free;
end;

end.
