{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-13  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.CodeCoverage.FileWriter;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   contnrs,
   Laz2_DOM;

type

   { TCodeCoverageXML }

   TCodeCoverageXML = class(TXMLDocument)
   private
      FRootNode: TDOMNode;
      FSourceFilename: string;
      FTotalsNode: TDOMNode;
      FTotalsClassesNode: TDOMNode;
      FTotalsMethodsNode: TDOMNode;
      FTotalsFunctionsNode: TDOMNode;
      FTotalsClassesCount: integer;
      FTotalsMethodsCount: integer;
      FTotalsFunctionsCount: integer;
   protected
      { Increments the class counter, updating the total field. }
      procedure IncrementClassesCount;
      procedure IncrementMethodsCount; /// tralala
      procedure IncrementFunctionsCount;
   public
      constructor Create;
      procedure Save;
      function FindFileNode(AFilename: string; ACreateIfMissing: boolean = True): TDOMNode;
      function FindClassNode(AFileNode: TDOMNode; AClassName: string; ACreateIfMissing: boolean = True; ALineStart: integer = 0): TDOMNode;
      function FindFunctionNode(AFileNode: TDOMNode; AFunctionName: string): TDOMNode;
      function FindMethodNode(AClassNode: TDOMNode; AMethodName: string): TDOMNode;
      function FindLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
      function AddLineToken(AFilename: string; ALineNumber: integer; ATokenName, ATokenValue: string): TDOMNode;
      function AddMethod(AFileNode: TDOMNode; AMethodName, ADeclaration: string; ALineStart: integer = 0): TDOMNode;
      procedure RegisterMethodCoveringCode(AFilename, AClassName, AMethodName: string);
      procedure RegisterFunctionCoveringCode(AFilename, AFunctionName: string);
      function FindCoverageNode(AFileNode: TDOMNode): TDOMNode;
      property RootNode: TDOMNode read FRootNode;
      property SourceFilename: string read FSourceFilename;
   end;

   { TCodeCoverageFiles }

   TCodeCoverageFiles = class(TObjectList)
   private
      function GetFile(AIndex: integer): TCodeCoverageXML;
   public
      function FindDocumentByFilename(AFilename: string): TCodeCoverageXML;
      property Files[AIndex: integer]: TCodeCoverageXML read GetFile;
   end;


implementation

uses
   Dialogs,
   laz2_XMLWrite;

{ TCodeCoverageFiles }

function TCodeCoverageFiles.GetFile(AIndex: integer): TCodeCoverageXML;
begin
   Result := TCodeCoverageXML(GetItem(AIndex));
end;

function TCodeCoverageFiles.FindDocumentByFilename(AFilename: string): TCodeCoverageXML;
var
   i: integer;
begin
   Result := nil;
   for i := 0 to Pred(Count) do begin
      if SameText(Files[i].SourceFilename, AFilename) then begin
         Result := Files[i];
         Exit;
      end;
   end;
   if not Assigned(Result) then begin
      Result := TCodeCoverageXML.Create;
      Result.FSourceFilename := AFilename;
      Self.Add(Result);
   end;
end;

{ TCodeCoverageXML }

{
  Increments the class counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementClassesCount;
begin
   Inc(FTotalsClassesCount);
   TDOMElement(FTotalsClassesNode).SetAttribute('total', IntToStr(FTotalsClassesCount));
end;

{
  Increments the method counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementMethodsCount;
begin
   Inc(FTotalsMethodsCount);
   TDOMElement(FTotalsMethodsNode).SetAttribute('total', IntToStr(FTotalsMethodsCount));
end;

{
  Increments the function counter, updating the total field.
}
procedure TCodeCoverageXML.IncrementFunctionsCount;
begin
   Inc(FTotalsFunctionsCount);
   TDOMElement(FTotalsFunctionsNode).SetAttribute('total', IntToStr(FTotalsFunctionsCount));
end;

constructor TCodeCoverageXML.Create;
begin
   inherited Create;
   FRootNode := Self.CreateElement('fpcunit');
   Self.AppendChild(FRootNode);
   FRootNode := Self.DocumentElement;
end;

procedure TCodeCoverageXML.Save;
begin
   WriteXMLFile(Self, FSourceFilename + '.xml');
end;

function TCodeCoverageXML.FindFileNode(AFilename: string; ACreateIfMissing: boolean): TDOMNode;
var
   iFile: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFile := 0 to Pred(FRootNode.ChildNodes.Count) do begin
      if FRootNode.ChildNodes[iFile].NodeName = 'file' then begin
         e := TDOMElement(FRootNode.ChildNodes[iFile]);
         if SameText(e.GetAttribute('name'), ExtractFilename(AFilename)) and SameText(e.GetAttribute('path'), ExtractFilePath(AFilename)) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not ACreateIfMissing then begin
      Exit;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('file');
      TDOMElement(Result).SetAttribute('name', ExtractFileName(AFilename));
      TDOMElement(Result).SetAttribute('path', ExtractFilePath(AFilename));
      RootNode.AppendChild(Result);

      FTotalsNode := Self.CreateElement('totals');
      Result.AppendChild(FTotalsNode);
      FTotalsClassesNode := Self.CreateElement('classes');
      TDOMElement(FTotalsClassesNode).SetAttribute('total', '0');
      FTotalsNode.AppendChild(FTotalsClassesNode);
      FTotalsMethodsNode := Self.CreateElement('methods');
      TDOMElement(FTotalsMethodsNode).SetAttribute('total', '0');
      FTotalsNode.AppendChild(FTotalsMethodsNode);
      FTotalsFunctionsNode := Self.CreateElement('functions');
      TDOMElement(FTotalsFunctionsNode).SetAttribute('total', '0');
      FTotalsNode.AppendChild(FTotalsFunctionsNode);
   end;
end;

function TCodeCoverageXML.FindClassNode(AFileNode: TDOMNode; AClassName: string; ACreateIfMissing: boolean; ALineStart: integer): TDOMNode;
var
   iFileChild: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'class' then begin
         e := TDOMElement(AFileNode.ChildNodes[iFileChild]);
         if SameText(e.GetAttribute('name'), AClassName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not ACreateIfMissing then begin
      Exit;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('class');
      TDOMElement(Result).SetAttribute('name', AClassName);
      TDOMElement(Result).SetAttribute('start', IntToStr(ALineStart));
      TDOMElement(Result).SetAttribute('executable', '0');
      AFileNode.AppendChild(Result);
      IncrementClassesCount;
   end;
end;

function TCodeCoverageXML.FindFunctionNode(AFileNode: TDOMNode; AFunctionName: string): TDOMNode;
var
   iFileChild: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'function' then begin
         e := TDOMElement(AFileNode.ChildNodes[iFileChild]);
         if SameText(e.GetAttribute('name'), AFunctionName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
end;

function TCodeCoverageXML.FindMethodNode(AClassNode: TDOMNode; AMethodName: string): TDOMNode;
var
   iMethod: integer;
   e: TDOMElement;
begin
   Result := nil;
   for iMethod := 0 to Pred(AClassNode.ChildNodes.Count) do begin
      if AClassNode.ChildNodes[iMethod].NodeName = 'method' then begin
         e := TDOMElement(AClassNode.ChildNodes[iMethod]);
         if SameText(e.GetAttribute('name'), AMethodName) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
end;

function TCodeCoverageXML.FindLineNode(AFilename: string; ALineNumber: integer): TDOMNode;
var
   iLine: integer;
   nCoverage: TDOMNode;
   e: TDOMElement;
begin
   Result := nil;
   nCoverage := FindCoverageNode(FindFileNode(AFilename));
   for iLine := 0 to Pred(nCoverage.ChildNodes.Count) do begin
      if nCoverage.ChildNodes[iLine].NodeName = 'line' then begin
         e := TDOMElement(nCoverage.ChildNodes[iLine]);
         if SameText(e.GetAttribute('nr'), IntToStr(ALineNumber)) then begin
            Result := e;
            Exit;
         end;
      end;
   end;
   if not Assigned(Result) then begin
      Result := Self.CreateElement('line');
      TDOMElement(Result).SetAttribute('nr', IntToStr(ALineNumber));
      nCoverage.AppendChild(Result);
   end;
end;

function TCodeCoverageXML.AddLineToken(AFilename: string; ALineNumber: integer; ATokenName, ATokenValue: string): TDOMNode;
var
   nLine: TDOMNode;
   dt: TDOMText;
begin
   nLine := FindLineNode(AFilename, ALineNumber);
   Result := Self.CreateElement('token');
   TDOMElement(Result).SetAttribute('name', ATokenName);
   if Length(ATokenValue) > 0 then begin
      dt := Self.CreateTextNode(ATokenValue);
      Result.AppendChild(dt);
   end;
   nLine.AppendChild(Result);
end;

function TCodeCoverageXML.AddMethod(AFileNode: TDOMNode; AMethodName, ADeclaration: string; ALineStart: integer): TDOMNode;
var
   iDot: integer;
   sClassName, sMethodName: string;
   nClass: TDOMNode;
begin
   ADeclaration := StringReplace(ADeclaration, #13, ' ', [rfReplaceAll]);
   ADeclaration := StringReplace(ADeclaration, #10, ' ', [rfReplaceAll]);
   while Pos('  ', ADeclaration) > 0 do begin
      ADeclaration := StringReplace(ADeclaration, '  ', ' ', [rfReplaceAll]);
   end;
   iDot := Pos('.', AMethodName);
   if iDot > 0 then begin
      Result := Self.CreateElement('method');
      sClassName := Copy(AMethodName, 1, Pred(iDot));
      sMethodName := Copy(AMethodName, Succ(iDot));
      nClass := Self.FindClassNode(AFileNode, sClassName, True, 0);
      TDOMElement(Result).SetAttribute('name', sMethodName);
      nClass.AppendChild(Result);
      IncrementMethodsCount;
   end else begin
      Result := Self.CreateElement('function');
      TDOMElement(Result).SetAttribute('name', AMethodName);
      AFileNode.AppendChild(Result);
      IncrementFunctionsCount;
   end;
   TDOMElement(Result).SetAttribute('start', IntToStr(ALineStart));
   TDOMElement(Result).SetAttribute('signature', ADeclaration);
   TDOMElement(Result).SetAttribute('coverage', '0');
   TDOMElement(Result).SetAttribute('executable', '0');
end;

procedure TCodeCoverageXML.RegisterMethodCoveringCode(AFilename, AClassName, AMethodName: string);
var
   nFile: TDOMNode;
   nClass: TDOMElement;
   nMethod: TDOMElement;
begin
   nFile := Self.FindFileNode(AFilename);
   nClass := TDOMElement(Self.FindClassNode(nFile, AClassName));
   nClass.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nClass.GetAttribute('executable'), 0))));
   nMethod := TDOMElement(Self.FindMethodNode(nClass, AMethodName));
   if Assigned(nMethod) then begin
      nMethod.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nMethod.GetAttribute('executable'), 0))));
   end else begin
      // TODO : convert into event
      ShowMessage('Unknown method covered: ' + AClassName + '.' + AMethodName + ' in ' + AFilename);
   end;
end;

procedure TCodeCoverageXML.RegisterFunctionCoveringCode(AFilename, AFunctionName: string);
var
   nFile: TDOMNode;
   nFunction: TDOMElement;
begin
   nFile := Self.FindFileNode(AFilename);
   nFunction := TDOMElement(Self.FindFunctionNode(nFile, AFunctionName));
   nFunction.SetAttribute('executable', IntToStr(Succ(StrToIntDef(nFunction.GetAttribute('executable'), 0))));
end;

function TCodeCoverageXML.FindCoverageNode(AFileNode: TDOMNode): TDOMNode;
var
   iFileChild: integer;
begin
   Result := nil;
   for iFileChild := 0 to Pred(AFileNode.ChildNodes.Count) do begin
      if AFileNode.ChildNodes[iFileChild].NodeName = 'coverage' then begin
         Result := AFileNode.ChildNodes[iFileChild];
         Exit;
      end;
   end;
   Result := Self.CreateElement('coverage');
   AFileNode.AppendChild(Result);
end;


end.
