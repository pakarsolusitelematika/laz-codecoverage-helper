{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-12  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit PepiMK.CodeCoverage.TestCase;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   fpcunit,
   testutils,
   testregistry,
   PepiMK.CodeCoverage;

type

   TCodeCoverageTestCase = class(TTestCase)
   published
      procedure TestFooBar;
   end;

implementation

(**
* @covers(TCodeCoverage.ParseSources)
* @covers(TCodeCoverage.ParseTests)
* @covers(TCodeCoverage.PreProcessCodeCoverage)
* @covers(TCodeCoverage.PostProcessCodeCoverage)
 *)
procedure TCodeCoverageTestCase.TestFooBar;
var
   c: TCodeCoverage;
begin
   c := TCodeCoverage.Create;
   try
      c.PreProcessCodeCoverage;
      c.PostProcessCodeCoverage;
   finally
      c.Free;
   end;
end;


initialization

   RegisterCodeCoverageTest(TCodeCoverageTestCase);
end.
