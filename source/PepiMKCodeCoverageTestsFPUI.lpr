{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(TODO : please fill in abstract here!)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-13  --  ---  Renamed from ram PepiMKCodeCoverageTestsFPUI to PepiMKCodeCoverageTestsFPUI
// 2017-06-13  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

program PepiMKCodeCoverageTestsFPUI;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

uses
   Interfaces,
   Forms,
   GuiTestRunner,
   PepiMK.CodeCoverage.TestCase,
   PepiMK.CodeCoverage,
   PepiMK.CodeCoverage.FileWriter,
   PepiMK.CodeCoverage.GuiTestRunner, PepiMK.CodeCoverage.GDB;

{$R *.res}

begin
   Application.Initialize;
   Application.CreateForm(TCodeCoverageGUITestRunner, TestRunner);
   Application.Run;
end.
