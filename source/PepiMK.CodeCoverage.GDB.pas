{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Parses Gnu Debugger files for source unit paths.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2017-06-14  pk  30m  Created unit.
// *****************************************************************************
   )
}

unit PepiMK.CodeCoverage.GDB;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Windows;

type

   { TGnuDebuggerFile }

   TGnuDebuggerFile = class
   private
      FStabStrings: TStringList;
      {
        Processes the content of the .stabstr section of the debugger file.

        @param(AStream A stream containing the section content.)
      }
      function ProcessStabStr(AStream: TStream): boolean;
   public
      constructor Create;
      destructor Destroy; override;
      {
        Load information from a Gnu Debugger (.gdb) file.

        @param(AFilename The name of the .gdb file to load.)
      }
      function LoadFromFile(AFilename: string): boolean;
      {
        Tries to find the full path of a unit.

        @param(AUnitName Takes the unit name, and returns it with path and appended .pas.)
        @return(True if unit was found.)
      }
      function GetUnitFullFilename(var AUnitName: string): boolean;
      {
        ListUnitFilenames returns all existing unit files referenced in the
        .gdb file.
      }
      procedure ListUnitFilenames(AList: TStrings);
   end;

implementation

uses
   imagehlp;

function SectionNameToString(ANameBytes: array of byte): ansistring;
var
   i: integer;
begin
   Result := '';
   for i := 0 to 7 do begin
      if ANameBytes[i] <> 0 then begin
         Result := Result + Chr(ANameBytes[i]);
      end else begin
         Exit;
      end;
   end;
end;

function RVAToPointer(ARelativeVirtualAddress: DWORD; const AnImage: TLOADED_IMAGE): Pointer;
var
   pDummy: PPIMAGE_SECTION_HEADER;
begin
   pDummy := nil;
   Result := ImageRvaToVa(AnImage.FileHeader, AnImage.MappedAddress, ARelativeVirtualAddress, pDummy);
end;

{ TGnuDebuggerFile }

function TGnuDebuggerFile.ProcessStabStr(AStream: TStream): boolean;
var
   sWorking: ansistring;
   c: AnsiChar;
begin
   FStabStrings.Clear;
   sWorking := '';
   c := #0;
   while AStream.Position < AStream.Size do begin
      AStream.Read(c, 1);
      if c = #0 then begin
         FStabStrings.Add(sWorking);
         sWorking := '';
      end else begin
         sWorking += c;
      end;
   end;
   FStabStrings.Add(sWorking);
end;

constructor TGnuDebuggerFile.Create;
begin
   FStabStrings := TStringList.Create;
end;

destructor TGnuDebuggerFile.Destroy;
begin
   FStabStrings.Free;
   inherited Destroy;
end;

function TGnuDebuggerFile.LoadFromFile(AFilename: string): boolean;
var
   pi: PLOADED_IMAGE;
   pish: PIMAGE_SECTION_HEADER;
   i: integer;
   sSection: string;
   ms: TMemoryStream;
begin
   Result := False;
   pi := ImageLoad(PChar(ExtractFileName(AFilename)), PChar(ExtractFilePath(AFilename)));
   if Assigned(pi) then begin
      try
         for i := 0 to Pred(pi^.FileHeader^.FileHeader.NumberOfSections) do begin
            pish := pi^.Sections;
            Inc(pish, i);
            sSection := SectionNameToString(pish^.Name);
            if SameText(sSection, '.stabstr') then begin
               ms := TMemoryStream.Create;
               try
                  ms.Size := pish^.SizeOfRawData;
                  Move(RVAToPointer(pish^.VirtualAddress, pi^)^, ms.Memory^, ms.Size);
                  ms.Seek(0, soFromBeginning);
                  Result := ProcessStabStr(ms);
               finally
                  ms.Free;
               end;
            end;
         end;
      finally
         ImageUnload(pi);
      end;
   end;
end;

function TGnuDebuggerFile.GetUnitFullFilename(var AUnitName: string): boolean;
var
   i: integer;
begin
   i := FStabStrings.IndexOf(AUnitName + '.pas');
   Result := (i > 0);
   if Result then begin
      AUnitName := FStabStrings[Pred(i)] + AUnitName + '.pas';
   end;
   {$IFDEF MSWindows}
   AUnitName := StringReplace(AUnitName, '/', '\', [rfReplaceAll]);
   {$ENDIF MSWindows}
end;

procedure TGnuDebuggerFile.ListUnitFilenames(AList: TStrings);
var
   i: integer;
   sUnitName: string;
begin
   // TODO : fill with all existing units
   for i := 1 to Pred(FStabStrings.Count) do begin
      if SameText('.PAS', RightStr(FStabStrings[i], 4)) then begin
         sUnitName := FStabStrings[Pred(i)] + FStabStrings[i];
         if FileExists(sUnitName) then begin
            {$IFDEF MSWindows}
            sUnitName := StringReplace(sUnitName, '/', '\', [rfReplaceAll]);
            {$ENDIF MSWindows}
            AList.Add(sUnitName);
         end;
      end;
   end;
end;

end.
